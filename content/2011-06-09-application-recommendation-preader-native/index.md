+++
title = "Application Recommendation: pReader Native"
aliases = ["2011/06/application-recommendation-preader.html"]
author = "peter"
date = "2011-06-09T15:13:00Z"
layout = "post"
[taxonomies]
tags = ["eBooks", "electronic reading", "HP webOS", "Palm", "Palm Pre Plus", "pReader", "webOS", "webOS 2.1.0"]
categories = ["software"]
authors = ["peter"]
+++

There is one big reason besides surfing and gaming that people are interested in tablets: Reading. Be it magazines or books, be it readers or publishing houses, everybody is exited about the new opportunities of digital publishing.
<!-- more -->
<img style="width:100%" src="preader.jpg" />

Actually you don't need a tablet or a special ereader device to read on a mobile device. You can read on your smartphone, too. While there are many options on Android (including the brilliant open source application <a href="http://www.fbreader.org/FBReaderJ/">FBreaderJ</a>) , it boils down to a few on webOS. 

One of these applications is the GPLv3 licensed pReader Native (<a href="http://preader.sourceforge.net/">SourceForge.net</a> / <a href="http://forums.precentral.net/homebrew-apps/274511-preader-native-alpha-release.html">PreCentral thread</a>), which is available in Preware (or via webOSQuickInstall). It is a rewrite of the original pReader application (which was homebrew, too), using the PDK features introduced with webOS 1.4.5. 

pReader native supports many popular E-Book formats, ePub, eReader (including DRM), PalmDOC + plain text. Installing via Preware shouldn't be to difficult, if you don't have Preware yet on your webOS device, get it quick, it's really worth it.

pReader works nicely. The UI may not be as fancy as the iBooks UI, but the application just works as it should, uses stock webOS UI widgets and thus doesn't break the look and feel. Settings are plenty, you can set colors and scrolling options&mdash;everything <b>I</b> need is there. Reading on the Pre works nicely with pReader Native&mdash;because the Pres&#8217; display is bright and has an acceptable pixel density. 

_Screenshots:_

<img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_131628.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_164842.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_164853.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_164901.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_164917.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_164935.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_164939.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_164953.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165004.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165010.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165015.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165021.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165025.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165036.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165047.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165105.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165112.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165125.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165200.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165211.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165220.png"  /><img border="0" style="height:480px;width:320px;margin:0.5em;" src="preader-native_2011-09-06_165242.png"  /></a>
