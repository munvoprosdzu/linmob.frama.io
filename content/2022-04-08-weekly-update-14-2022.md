+++
title = "Weekly Update (14/2022): Maui Shell Alpha, Mobian goes Tow-Boot, Suspend on the Librem 5 and AVMultiPhone is back!"
draft = false
date = "2022-04-08T21:57:00Z"
updated="2022-04-09T09:26:00Z"
[taxonomies]
tags = ["PinePhone","PinePhone Pro", "LinuxPhoneApps","Librem 5",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
update_note="Added the Capyloon release I missed."
+++

Another calm week, where I spent most of my free time on LinuxPhoneApps has gone by. Here's what happened:
<!-- more -->

_Commentary in italics._

###  Hardware enablement
* David Hamner for Purism: [Librem 5 Suspend Preview](https://puri.sm/posts/librem-5-suspend-preview/). _I've briefly tried it, and it works for me :-) Feels a lot like when Crust was new in PinePhone-Land._

### Software progress

#### GNOME ecosystem
* Phosh is adding more swipy stuff:  [Swipe-able quick settings on the lock screen](https://twitter.com/GuidoGuenther/status/1510535706950746114) and [Home-bar/overview](https://twitter.com/GuidoGuenther/status/1511660423455596552).
* Chris Davis: [Plans for GNOME 43 and Beyond](https://blogs.gnome.org/christopherdavis/2022/04/03/plans-for-gnome-43-and-beyond/). _Nice plans!_
* This Week in GNOME: [#38 Among Toasts And Tabs](https://thisweek.gnome.org/posts/2022/04/twig-38/).

#### Plasma/Maui ecosystem
* Nitrux: [Maui Shell Alpha Release](https://nxos.org/maui/maui-shell-alpha-release/).
  * Liliputing: [Maui Shell Alpha released (convergent desktop environment for Linux phones and PCs)](https://liliputing.com/2022/04/maui-shell-alpha-released-convergent-desktop-environment-for-linux-phones-and-pcs.html).
* Nate Graham: [This week in KDE: Get ready for a big one!](https://pointieststick.com/2022/04/01/this-week-in-kde-get-ready-for-a-big-one/).
* Volker Krause: [February/March in KDE Itinerary](https://www.volkerkrause.eu/2022/04/02/kde-itinerary-february-march-2022.html).
* Samarth Raj: [Sok’22 Week 11: Finalizing the Left-Right Click Activity](https://samarthrajwrites.wordpress.com/2022/04/08/sok22-week-11.-finalizing-the-left-right-click-activity/)
* Phoronix: [Qt 6.3 To Boast Improved Wayland Integration, Easily Allows Custom Shell Extensions](https://www.phoronix.com/scan.php?page=news_item&px=Qt-6.3-Wayland-Custom).

#### Sailfish OS
* martyone: [[Release notes] Sailfish SDK 3.9](https://forum.sailfishos.org/t/release-notes-sailfish-sdk-3-9/10941).

#### Sxmo
* Anjandev Momi: [Breaking Change in master branch for non-pinephone users](https://lists.sr.ht/~mil/sxmo-announce/%3C877d80lq1k.fsf%40momi.ca%3E).

#### Capyloon
* Capyloon have [published another release today](https://capyloon.org/releases.html#april-08), improving Web Extensions support, UX cleanup and implementing app management commands for Linux Phones.

#### Distro news
* [Mobian images for the PinePhone and PinePhone Pro are no longer shipped with device specific uboot bootloaders](https://wiki.mobian-project.org/doku.php?id=install-linux), users will need to get [tow-boot](https://tow-boot.org/) set up instead.
* Archivista: [That’s why the Archivista phone is now in its second generation](https://archivista.ch/cms/en/news/avmultiphone-v2/). _The previous release of this postmarketOS based "putting Mate desktop on the PinePhone" distro was in July of 2020._
* postmarketOS Edge: [pinephone: suspend is broken for some users](https://postmarketos.org/edge/2022/04/06/pinephone-suspend/).
* Dan Johansen: [This month in Manjaro (March 2022)](https://blog.strits.dk/this-month-in-manjaro-march-2022/).

### Worth reading

#### Ambitious Projects
* James Livesey on Dev.to: [Project Prism: on the road to building an open smartphone](https://dev.to/liveg/project-prism-on-the-road-to-building-an-open-smartphone-50lg). _Ambitious project, great to see accessibility among the project goals!_

#### PinePhone Keyboard Software
* xnux.eu (Megi): [Pinephone keyboard power manager](https://xnux.eu/log/#065).

#### Sailfish OS and its legacy
* Camden Bruce: [Sailfish OS — Meego’s legacy](https://medium.com/@camden.o.b/sailfish-os-meegos-legacy-d55282fd362b). _Nice posts, I love it when history is being discussed!_

#### App lists and such
* LinuxPhoneApps blog: [New apps of LINMOBapps, Q1/2022 🎉](https://linuxphoneapps.org/blog/new-listed-apps-q1-2022/). _The Q2 post will have a different name (s/LINMOBapps/LinuxPhoneApps/g), but for this one this title felt appropriate!_

### Worth watching

#### PinePhone (Pro)
* socketwench:[PinePhone Pro Week 2 Review](https://www.youtube.com/watch?v=oNxpIFFLa6g). _Fun war stories, and accurate words on Plasma Mobile._
* Drew Naylor: [How to Switch TTYs with the PinePhone Keyboard Case (systemd-only method)](https://www.youtube.com/watch?v=XXX3ntsgTSE). _Nice tutorial!_
* Bill Degener: [Inside Pinephone Convergence dock Part 2](https://www.youtube.com/watch?v=6CiTg8PB8s4).
* Ivon Huang: [自由開源的Linux手機: PinePhone開箱評測](https://www.youtube.com/watch?v=xlIBJyGESmQ)

#### Librem 5 Suspend
* Purism: [Librem 5 Suspend Preview](https://www.youtube.com/watch?v=XbnrhIV0mDY).

#### postmarketOS Installation
* edi194: [Installing PostmarketOS on OnePlus 6T](https://www.youtube.com/watch?v=RhGDvUC6CEo).

#### Ubuntu Touch Installation
* Solution Engineer: [How To Install Ubuntu Touch Os In Asus Zenfone Max Pro M1 || Step By Step.](https://www.youtube.com/watch?v=GInHNCPZGeg)
* Scientific Perspective: [Ubuntu Touch & multirom installation | multibooting | POCO F1 [No voice]](https://www.youtube.com/watch?v=D6H__yAqaqM).

#### Ubuntu Touch Details & Dev
* totodesbois100: [Telephony stack overview on UbuntuTouch. How messaging-app, dialer-app talk to ofono](https://www.youtube.com/watch?v=IGgbEhTrmvg).
* totodesbois100: [UbuntuTouch: Dive into music file import issue with music application.](https://www.youtube.com/watch?v=1F92T6568bg)
* totodesbois100: [Ubuntu Touch, a small journey into history service. Example with Dialer-app,:HistoryManager QML Type](https://www.youtube.com/watch?v=z-DE4nOBC1E).

#### Linux Phone Gaming
* Cha14ka: [Half-Life on PinePhone Pro](https://www.youtube.com/watch?v=YDPaIgdF2CM)
* Ivon Huang: [Linux Android Waydroid gaming: Fate/Grand Order](https://www.youtube.com/watch?v=hDbVMUvIIPA)

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!
