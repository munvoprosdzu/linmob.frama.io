+++
title = "LINMOBapps: Additions in March and April 2021 and future plans"
aliases = ["2021/05/07/linmobapps-additions-march-april-2021-and-plans.html"]
author = "Peter"
date = "2021-05-08T10:37:18Z"
[taxonomies]
authors = ["peter"]
tags = ["PinePhone", "Librem 5", "Apps", "LINMOBapps", "LinuxPhoneApps"]
categories = ["projects"]
+++

_Two months ago, I decided to first post a review of what was added in the past two months. I also want to share my further plans that aim to replace the list in its current form._

<!-- more -->

## Apps that have been added

Let's start with the apps that have been added to [LINMOBapps](https://linmobapps.frama.io/) in the past two months. _Sadly, no screenshots again, but I promise to take some of all the apps that are being added going forward!_

### March

* [Idok Remote](https://gitlab.com/The3DmaN/idokremote), a simple Kodi remote that supports multiple Kodi instances.[^1]
* [Wordbook](https://github.com/fushinari/Wordbook) is a dictionary application made for Linux using Python and GTK 3.
* [AudioTube](https://invent.kde.org/jbbgameich/audiotube) can search YouTube Music, list albums and  artists, play automatically generated playlists, albums and allows to put your own playlist together.[^2]
* [Elisa](https://invent.kde.org/multimedia/elisa), a beautiful no-nonsense music player with online radio support.[^2]
* [Sound Recorder](https://gitlab.gnome.org/GNOME/gnome-sound-recorder/), a simple and modern sound recorder.
* [wake-mobile](https://gitlab.gnome.org/kailueke/wake-mobile), a proof-of-concept alarm app that uses systemd timers to wake up the system. _See also Birdie below._
* [Numberstation](https://git.sr.ht/~martijnbraam/numberstation), a Gnome Authenticator clone. It generates 2fa tokens based on secrets installed. It also registers as URI-handler for otpauth:// UTLs so they can be added from Megapixels. 
* [Thumbdrives](https://git.sr.ht/~martijnbraam/thumbdrives), a thumbdrive and ISO emulator for phones. _Another super helpful app by Martijn Braam.
* [Sums](https://gitlab.com/leesonwai/sums), a simple GTK postfix calculator that adheres to GNOME's human-interface guidelines. It is designed to be keyboard-driven and aims to feel natural to interact with by recognising English-language input as mathematical constants and operations.
* [Characters](https://gitlab.gnome.org/GNOME/gnome-characters) is a simple utility application to find and insert unusual characters. _Looking for Emoji? This apps helps._
* [Mousai](https://github.com/SeaDve/Mousai), a simple application for identifying songs. _The household name for this task is Shazam._


### April

* [postmarketOS tweaks](https://gitlab.com/postmarketOS/postmarketos-tweaks), an Application for exposing extra settings easily on mobile platforms. _Yet another great application by Martijn!_
* [Public Transport](https://gitlab.com/terence97/publictransport) brings real time information for public transportation (currently in Germany) to Linux phones/the Linux desktop.
* [Simple Diary ](https://github.com/johan-bjareholt/simple-diary-gtk), a simple and lightweight diary app.
* [Pamac](https://gitlab.manjaro.org/applications/pamac) a graphical Package Manager for Manjaro Linux with Alpm, AUR, Appstream, Flatpak and Snap support.
* [Telegrand](https://github.com/melix99/telegrand) is a GTK4 Telegram client built to be well integrated with the GNOME desktop environment. _OpenSUSE packaged it recently!_
* [Lith](https://github.com/LithApp/Lith) is a multiplatform WeeChat Relay client, allowing you to connect to your running WeeChat instance from anywhere, be it your phone or your desktop computer.
* [Khronos](https://github.com/lainsce/khronos/), an inobtrusive time tracker for your tasks. _Sadly deep sleep interferes with time tracking on the PinePhone, but other than that: Nice app!_
* [PineBattery](https://github.com/JasonG-FR/PineBattery), a GTK app for monitoring the PinePhone and PineTab battery.
* [Birdie](https://github.com/Dejvino/birdie), a wakeup Alarm App for a Linux Phone. _[It works!](https://twitter.com/linmobblog/status/1386560286983864321)_
* [DayKountdown](https://invent.kde.org/plasma-mobile/daykountdown) is a simple date countdown app written for KDE Plasma, using Kirigami.
* [OptiImage](https://invent.kde.org/carlschwan/optiimage), an image optimizer based on optipng.
* [Web Archives](https://github.com/birros/web-archives) is a web archives reader offering the ability to browse offline millions of articles from large community projects such as  Wikipedia or Wikisource. _Only supports ZIM files, but works pretty great for reading e.g. Wikipedia offline!_
* [Clapper](https://github.com/Rafostar/clapper) is a GNOME media player built using GJS with GTK4 toolkit and powered by GStreamer with OpenGL rendering.

## The Game list has been taken on and has a future now!
_Moxvallix_ took over the [game list](https://linmobapps.frama.io/games.html) and has done a great job at adding games to it!

## More Changes

Little happened, in part because I was busy working on this blog. I've created a [Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org), and since yesterday the appstream:// links [finally work](https://fosstodon.org/@linmob/106195688191823800)&mdash;but these changes are to be discussed in a later update.

## The Future: LinuxPhoneApps.org

The app list, as is, has its deficiencies: It's tough to maintain, and the "it's just one .csv file" nature seems to be intimidating.
While it is searchable, the search function is apparently not discoverable enough. Also, if you want to share one application from it on social media, you just can't do it in a good way. More importantly, it's really slow at loading on the devices it targets, due the use of lots of client side JavaScript, and worse yet, it gets a tiny bit slower to load with every app that gets added. 

### Scope

Replacing it, on the other hand, is not an easy task, for the number of apps alone. It needs thought, and external input, as there are three audiences this list should cater to:
* developers, 
  * to find out if instead of just scratching ones own itch, there's a project with a similar goal, that might appreciate help,
  * to publicize the app one worked on,
  * to find people that can help with e.g. new features or design,
* distribution maintainers, 
  * that want to package software for their users to provide a good experience,
  * find what competing distributions package,
* (prospective) users
  * that want to get a good idea of available apps, 
  * what these apps look like,
  * which features they already offer and which features are planned.

I, personally, am only in the third group. _Therefore, get in touch, if I've missed something, or just [contribute to the above list anonymously here](https://pad.hacc.space/6KZJFltXSHSMCa-4NaBYag?edit)._

### Technical implementation

A long time ago, cahfofpai (who created [MGLapps](https://mglapps.frama.io)) and I came up with a draft of getting the list moved over to [Hugo](https://gohugo.io). This draft is what I've been spending some time on since, although I will likely rather use [Zola](https://www.getzola.org/), which may be less suitable than Hugo, but has the benefit that I somewhat understand how it works. I hope to get a basic implementation of this done in the next weeks.

One key challenge is that both Hugo and Zola, being static site generators, are less dynamic, which require workarounds, so that the new solution is actually better. One way to do this would be to somehow keep the current list around and maintained, but unless this can be seriously automized, it's not a realisitic goal. Ideas welcome!

#### Automatization

Speaking of automization: With more and more apps listed, manually monitoring changes and progress gets more and more of a serious burden. Thankfully, this can be automized for some apps, but sadly it's one of the tasks that I don't feel up to as it's beyond my current abilities: 

This tasks is the creation of a tool that downloads structured project information that you may know as metainfo.xml or appdata.xml files that follow the [FreeDesktop.org AppStream Metadata specification](https://freedesktop.org/software/appstream/docs/chap-Quickstart.html) and then alerts for changes or, even better, updates the specific fields in the apps markdown file TOML or YAML frontmatter automatically.

Another task that's more and more of a burden and the LINMOBapps doesn't do a good job at is checking for packages. Thankfully, [Repology](https://repology.org/) exists, and will be used for each app individually.

#### Hosting

I have been considering hosting the code behind this new project (in order to get more contributions and increase visibility) on platforms that are popular, and have a working search for hosted projects. Obvious candidates would be [Sourcehut](https://sourcehut.org/) and [GitHub](https://github.com), because both have quite different audiences, and we would get a mailing list for free.


### How can you help?

Please join the [discussion on Matrix](https://matrix.to/#/#linuxphoneapps:matrix.org) and contribute your input regarding [audiences](https://pad.hacc.space/6KZJFltXSHSMCa-4NaBYag?edit) and [implementation](https://pad.hacc.space/xKq47oRxQqSuhz21z8lnEg?view). _I will post more on the topic once I have a rough prototype of my Zola implementation ready!_


[^1]: Thanks to The3DmaN for adding this apps!

[^2]: Thanks to cahfofpai for adding these! You may remember him from his work on [MGLapps](https://mglapps.frama.io).
